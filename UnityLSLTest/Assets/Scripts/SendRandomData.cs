using UnityEngine;
using System;
using System.Threading;
using LSL;

class SendRandomData : MonoBehaviour
{
    System.Random rnd;
    liblsl.StreamOutlet outlet;
    void Awake()
    {
        rnd = new System.Random();
        // create stream info and outlet
        liblsl.StreamInfo info = new liblsl.StreamInfo("BioSemi", "EEG", 8, 100, liblsl.channel_format_t.cf_float32, "sddsfsdf");
        outlet = new liblsl.StreamOutlet(info);
    }

    void Update()
    {
        float[] data = new float[8];
        // generate random data and send it
        for (int k = 0; k < data.Length; k++)
            data[k] = rnd.Next(-100, 100);
        outlet.push_sample(data);
        Debug.Log("Pushed sample");
    }
}
