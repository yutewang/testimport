﻿using UnityEngine;
using System;
using System.Threading;
using LSL;

class SendMouseData : MonoBehaviour
{
    System.Random rnd;
    liblsl.StreamOutlet outlet;
    void Awake()
    {
        rnd = new System.Random();
        // create stream info and outlet
        liblsl.StreamInfo info = new liblsl.StreamInfo("BioSemi", "EEG", 8, 100, liblsl.channel_format_t.cf_float32, "sddsfsdf");
        outlet = new liblsl.StreamOutlet(info);
    }

    void FixedUpdate()
    {
        float[] data = new float[8];
        data[0] = Input.mousePosition.x;
        data[1] = Input.mousePosition.y;
        data[2] = Input.GetMouseButton(0) ? 1 : 0;
        data[3] = Input.GetMouseButton(1) ? 1 : 0;
        data[4] = Input.GetMouseButton(2) ? 1 : 0;
        // generate random data and send it
        for (int k = 5; k < data.Length; k++)
            data[k] = rnd.Next(-100, 100);
        outlet.push_sample(data);
    }
}
